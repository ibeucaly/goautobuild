module github.com/ibeucaly/gobuildserver

go 1.12

require (
	github.com/fsnotify/fsnotify v1.4.7
	github.com/gin-gonic/gin v1.4.0
	golang.org/x/sys v0.0.0-20190531175056-4c3a928424d2 // indirect
)
