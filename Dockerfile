FROM ubuntu:latest

# Install utilities
RUN apt-get update --fix-missing && apt-get -y upgrade \
 && apt-get install -y sudo curl wget unzip git gnupg

#ENV DEBIAN_FRONTEND noninteractive
#RUN apt-get install -y tzdata
#ENV TIMEZONE Asia/Tokyo
#RUN ln -fs /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
#RUN dpkg-reconfigure --frontend noninteractive tzdata

# Clean(reduce image size)
RUN apt-get clean

# Install Go Compiler
ENV GOLANG_VERSION 1.12.5
RUN wget -q -O golang.tgz https://golang.org/dl/go${GOLANG_VERSION}.linux-amd64.tar.gz \
 && tar -C /usr/local -xzf golang.tgz \
 && rm golang.tgz
#RUN mkdir /go
#RUN mkdir /go/bin
#ENV GOPATH /go
ENV PATH=$PATH:/usr/local/go/bin \
 GO111MODULE=on

RUN go get github.com/githubnemo/CompileDaemon \
 && go install github.com/githubnemo/CompileDaemon

# Switch execute user
RUN useradd -m -s /bin/bash appuser \
 && echo "appuser:appuser" | chpasswd
USER appuser
WORKDIR /home/appuser

# Clone and build
RUN git clone https://bitbucket.org/ibeucaly/goautobuild/
#RUN cd goautobuild &&\
   # go mod init goautobuild &&\
   # go build

# Copy build script
#COPY --chown=appuser:appuser build.sh /home/appuser/goautobuild
